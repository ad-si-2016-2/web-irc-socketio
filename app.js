// configuracao do express
var app = require('express')();  // módulo express
var server = require('http').Server(app);
var cookieParser = require('cookie-parser');  // processa cookies
app.use(cookieParser());
var bodyParser = require('body-parser');  // processa corpo de requests
app.use(bodyParser.urlencoded( { extended: true } ));

// configuracao do socket.io
var socketio = require('socket.io')(server);
var socketio_cookieParser = require('socket.io-cookie');
socketio.use(socketio_cookieParser);
server.listen(8080, function() {
    console.log('Listening at: http://localhost:8080');
});
socketio.listen(server);

var irc = require('irc');

var fs = require('fs');
var path = require('path');	// módulo usado para lidar com caminhos

var proxies = {}; // mapa de proxys
var proxy_id = 1;

var Proxy = function(servidor, nick, canal) {
	this.id = proxy_id++;
	this.servidor = servidor;
	this.nick = nick;
	this.canal = canal;
	this.socket = null;
	this.url = null;
	this.irc_client = new irc.Client(
			servidor, 
			nick,
			{channels: [canal],});
	proxies[this.id] = this;

	socketio.on('connection', function (socket) {
	    // o proxy_id pode ser obtido a partir do websocket
	    var proxy_id = socket.request.headers.cookie.id;
	    // executa somente se o socket ainda não foi criado
	    if ( ! proxies[proxy_id].socket ) {
		    proxies[proxy_id].socket = socket;
		    socket.on('message', function (msg) {
			if (msg.substring(0,1) == '/') {
				var proxy_id = this.request.headers.cookie.id;
				proxies[proxy_id].processar_comando(msg);
			}
			else {
				var proxy_id = this.request.headers.cookie.id;
				// console.log('Message Received: '+msg+' de proxy_id: '+proxy_id);
				var client = proxies[proxy_id].irc_client;
				client.say(client.opt.channels[0], msg);
			}
		    });
	    }
	});
	this.processar_comando = function(msg) {
		if (msg == '/lusers' ) {
			var canal = this.irc_client.opt.channels[0];
			this.irc_client.send('names', canal);
		}
	};
	// inclui a propriedade proxy_id no irc_client
	this.irc_client.proxy_id = this.id;

	this.irc_client.addListener('message'+canal, function (from, message) 	 {
	    var socket = proxies[this.proxy_id].socket;
	    socket.emit('message', {"timestamp":Date.now(), 
			"nick":from,
			"msg":message} );
	});
	this.irc_client.addListener('names', function(channel, nicks) {
	    var lista = channel+': '+JSON.stringify(nicks);
	    console.log('listagem de nicks do canal:'+lista);
	    var socket = proxies[this.proxy_id].socket;
	    socket.emit('message', {"timestamp":Date.now(), 
					"nick":'status',
					"msg":"listagem de nicks do canal "+lista});
	});
	this.irc_client.addListener('error', function(message) {
	    console.log('error: ', message);
	});
	this.irc_client.addListener('registered', function(message) {
	  console.log('conectado:'+JSON.stringify(message));
	});
	this.irc_client.addListener('join', function(message) {
		if ( ! this.url ) {
			var socket = proxies[this.proxy_id].socket;
			this.url = 'irc://'+this.opt.nick+'@'+
						this.opt.server+'/'+
						this.opt.channels[0];
			socket.emit('message', {"timestamp":Date.now(), 
					"nick":'status',
					"msg":"conectado em "+this.url});
		}
	});
}


app.get('/', function (req, res) {
  if ( req.cookies.servidor && req.cookies.nick  && req.cookies.canal ) {
	var proxy =	new Proxy(
				req.cookies.servidor,
				req.cookies.nick, 
				req.cookies.canal);
        res.cookie('id', proxy.id);	
  	res.sendFile(path.join(__dirname, '/index.html'));
  }
  else {
        res.sendFile(path.join(__dirname, '/login.html'));
  } 
});

app.post('/login', function (req, res) { 
   res.cookie('nick', req.body.nome);
   res.cookie('canal', req.body.canal);
   res.cookie('servidor', req.body.servidor);
   res.redirect('/');
});

